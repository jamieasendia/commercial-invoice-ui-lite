﻿namespace Commercial_Invoice_UI.Models
{
    public class SelectOption
    {
        public string Label { get; set; }
        public int Value { get; set; }
    }
}