﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Security.Claims;
using System.Threading.Tasks;
using AsendiaShared.DTO.Entities;
using Commercial_Invoice_CodeLib.Model;
using Commercial_Invoice_CodeLib.Service;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NLog.Web.AspNetCore.LayoutRenderers;

namespace Commercial_Invoice_UI.Controllers
{
    public class AccountController : Controller
    {
        public readonly ILogger<AccountController> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly IAuthService _authService;

        public AccountController(ILogger<AccountController> logger, IMemoryCache memoryCache, IAuthService authService)
        {
            _logger = logger;
            _memoryCache = memoryCache;
            _authService = authService;
        }
        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

     /*   private async Task<User> GetLogin(string userName, string password)
        {
            User user = null;
            if (String.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password)) return user;
            user = await _authService.GetUserByLogin(userName);
            if (user!=null)  return user;
            return user;
        }*/


        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel loginModel, string returnUrl=null)//(string username, string password, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            var user = await _authService.GetUserByLogin(loginModel.Username);//await GetLogin(loginModel.Username, loginModel.Password);
            // Normally Identity handles sign in, but you can do it directly
            if (user == null)
            {
                ModelState.AddModelError(nameof(LoginModel.Username),"Login details are incorrect");
                return View();
            }

            if (user.Password != loginModel.Password)
            {
                ModelState.AddModelError(nameof(LoginModel.Password),"Login details are incorrect");
                _logger.LogInformation($"Incorrect Password for  {loginModel.Username}");
                return View();
            }

            var claimBranches = new List<Claim>();

            foreach (var branch in user.Branches)
            {
                var claim = new Claim(ClaimTypes.Role, branch.BranchID.ToString());
                claimBranches.Add(claim);
            }
               
 
            await HttpContext.SignInAsync(new ClaimsPrincipal(new ClaimsIdentity(claimBranches, "Cookies", "user", "role")));
            _logger.LogInformation($"Login complete for  {loginModel.Username} - Branches {JsonConvert.SerializeObject(claimBranches)}");
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return Redirect("/");
            }
                
        }

        public IActionResult AccessDenied(string returnUrl = null)
        {
            return View();
        }

        //[ValidateAntiForgeryToken]
        [Route("Home/Logout")]
        [Route("Account/Logout")]
        public async Task<IActionResult> Logout()
        {
            if (HttpContext.User.Identity != null && HttpContext.User.Identity.IsAuthenticated)
            {
                await HttpContext.SignOutAsync();
            }

            return Redirect("/");
        }


        //[ValidateAntiForgeryToken]
        [Route("Home/ForgotPassword")]
        [Route("Account/ForgotPassword")]
        public async Task<IActionResult> ForgotPassword()
        {
            var model = new ForgotPasswordModel();
            return View(model);
        }

        public async Task<IActionResult> ResetPasswordConfirmation(ForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _authService.TryResetPassword(model.Email);
                
            }

            return View();
        }
    }
}
