﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commercial_Invoice_CodeLib.Helpers;

namespace Commercial_Invoice_UI.Controllers
{
    public class BaseController : Controller
    {
        protected IEnumerable<int> GetAllowedBranchIds()
        {
            IEnumerable<int> branchIds = HttpContext.User.Claims.GetBranches().Select(int.Parse);
            return branchIds;
        }

        protected string GetUserName()
        {
            return HttpContext.User.Identity?.Name;
        }
    }
}
