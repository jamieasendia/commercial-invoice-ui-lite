﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commercial_Invoice_CodeLib.Service;
using Commercial_Invoice_UI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace Commercial_Invoice_UI.Controllers
{
    [Authorize]
    public class LookupController : BaseController
    {
        private readonly ILookupService _lookupService;
        private readonly ILogger<LookupController> _logger;
        private readonly IMemoryCache _memoryCache;

        public LookupController(ILookupService lookupService, ILogger<LookupController> logger, IMemoryCache memoryCache)
        {
            _lookupService = lookupService;
            _logger = logger;
            _memoryCache = memoryCache;
        }
        // GET
     
        //[ValidateAntiForgeryToken]
        [HttpPost]
        [Route("Lookup/SearchParentRetailer")]
        public async Task<IEnumerable<string>> SearchParentRetailer( string searchQuery)
        {
            IEnumerable<string> searchResult = new List<string>();
            if (searchQuery is null) return searchResult;
            if (!this._memoryCache.TryGetValue("PARENTRETAILERS", out List<string> cacheEntry))
            {
                {searchResult = await _lookupService.GetParentRetailerName(searchQuery);}
            }
            else
            {
                searchResult = cacheEntry.FindAll(x => x.ToLowerInvariant().StartsWith(searchQuery.ToLowerInvariant()));
            }

  
            return searchResult;
        }

        //[ValidateAntiForgeryToken]
        [HttpPost]
        [Route("Lookup/SearchRetailer")]
        public async Task<IEnumerable<string>> SearchRetailer( string searchQuery)
        {
            IEnumerable<string> searchResult = new List<string>();
            if (searchQuery is null) return searchResult;
            if (!this._memoryCache.TryGetValue("RETAILERS", out List<string> cacheEntry))
            {
                {searchResult = await _lookupService.GetRetailerName(searchQuery);}
            }
            else
            {
                searchResult = cacheEntry.FindAll(x => x.ToLowerInvariant().StartsWith(searchQuery.ToLowerInvariant()));
            }

  
            return searchResult;
        }


       // [ValidateAntiForgeryToken]
        [HttpGet]
        [Route("Lookup/GetCountries")]
        public async Task<IEnumerable<SelectOption>> GetCountries()
        {
            IEnumerable<Country> result;

            if (!this._memoryCache.TryGetValue("COUNTRIES", out List<Country> cacheEntry))
            {
                {
                    result = await _lookupService.GetAllCountries();
                }
            }
            else
            {
                result = cacheEntry;
            }

            return result.OrderBy(x => x.CountryName).Select(x =>
                new SelectOption
                    {Label = x.CountryName, Value = x.CountryId});
        }
    }
}