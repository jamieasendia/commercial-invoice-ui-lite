﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Commercial_Invoice_CodeLib.Model;
using Commercial_Invoice_CodeLib.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Wkhtmltopdf.NetCore;
using Wkhtmltopdf.NetCore.Options;

namespace Commercial_Invoice_UI.Controllers
{
    [Authorize]
    public class InvoiceController : BaseController
    {
        private readonly IInvoiceService _invoiceService;
        private readonly IGeneratePdf _generatePdf;
        private readonly ISearchService _searchService;
        private readonly ILogger<InvoiceController> _logger;

        public InvoiceController(IGeneratePdf generatePdf, IInvoiceService invoiceService, ISearchService searchService, ILogger<InvoiceController> logger)
        {
            _generatePdf = generatePdf;
            _invoiceService = invoiceService;
            _searchService = searchService;
            _logger = logger;
        }

        //[ValidateAntiForgeryToken]
        [HttpPost]
        [Route("Invoice/GetInvoiceAsPdf")]
        public async Task<IActionResult> GetInvoiceAsPdf(int parcelId)
        {
            var ids = new List<int>();
            ids.Add(parcelId);
            _logger.LogInformation($"Get Invoice initiated {GetUserName()} - {JsonConvert.SerializeObject(ids)}");
     
            IEnumerable<CommercialInvoiceViewModel> model =
                await _invoiceService.GetCommercialInvoiceViewModels(ids, GetAllowedBranchIds());

            //return await _generatePdf.GetPdf("/Views/Invoice/Default.cshtml",model);
            
            var pdf = await _generatePdf.GetByteArray("/Views/Invoice/Default.cshtml", model);
            _logger.LogInformation($"Get Invoices complete {GetUserName()}");
            return File(pdf, "application/pdf", GetFileName(model));
        }

        //[ValidateAntiForgeryToken]
        [HttpPost]
        [Route("Invoice/GetInvoicesAsPdf")]
        public async Task<IActionResult> GetInvoicesAsPdf(string data)
        {

            IEnumerable<int> parcelIds = JsonConvert.DeserializeObject<IEnumerable<int>>(data);
            _logger.LogInformation($"Get Invoices initiated {GetUserName()} - {JsonConvert.SerializeObject(parcelIds)}");
            IEnumerable<CommercialInvoiceViewModel> model = await _invoiceService.GetCommercialInvoiceViewModels(parcelIds,GetAllowedBranchIds());


            //return await _generatePdf.GetPdf("/Views/Invoice/Default.cshtml",model);
            
            var pdf = await _generatePdf.GetByteArray("/Views/Invoice/Default.cshtml", model);
            _logger.LogInformation($"Get Invoices complete {GetUserName()}");
            return File(pdf, "application/pdf", GetFileName(model));
        }

         //[ValidateAntiForgeryToken]
        [HttpGet]
        [Route("Invoice/GetInvoice")]
        public async Task<IActionResult> GetInvoice(int parcelId)
        {
            _logger.LogInformation($"View Invoices initiated {GetUserName()} - {JsonConvert.SerializeObject(parcelId)}");
            IEnumerable<CommercialInvoiceViewModel> model = new List<CommercialInvoiceViewModel>();
            if (parcelId > 0)
            {

                model = new List<CommercialInvoiceViewModel> {await _invoiceService.GetCommercialInvoice(parcelId,GetAllowedBranchIds())};

            }
            _logger.LogInformation($"View Invoice complete {GetUserName()}");
            return PartialView("/Views/Invoice/DefaultScreen.cshtml",model.ToList());
        }

        // [ValidateAntiForgeryToken]
        [HttpPost]
        [Route("Invoice/GetInvoices")]
        public async Task<IActionResult> GetInvoices( string data)
        {
            IEnumerable<int> parcelIds = JsonConvert.DeserializeObject<IEnumerable<int>>(data);
            _logger.LogInformation($"View Invoices initiated {GetUserName()} - {JsonConvert.SerializeObject(parcelIds)}");
            IEnumerable<CommercialInvoiceViewModel> model = await _invoiceService.GetCommercialInvoices(parcelIds,GetAllowedBranchIds());
            _logger.LogInformation($"View Invoices complete {GetUserName()}");
            return PartialView("/Views/Invoice/DefaultScreen.cshtml",model.ToList());
        }

        
        //[ValidateAntiForgeryToken]
        [HttpPost]
        [Route("Invoice/ViewAll")]
        public async Task<IActionResult> ViewAll(string data)
        {
            
            AdvancedQuery searchQuery = JsonConvert.DeserializeObject<AdvancedQuery>(data);
            Task<IEnumerable<CommercialInvoiceViewModel>> model = null;// = new List<CommercialInvoiceViewModel>();
            _logger.LogInformation($"View All Invoices initiated {GetUserName()} - {JsonConvert.SerializeObject(searchQuery)}");
            if (searchQuery is null) return null;
            IEnumerable<int> parcelIds = new List<int>();
            if (!searchQuery.UseAdvancedSearch)
            {
                parcelIds = await _searchService.GetParcelIdsFromSimpleSearch(searchQuery,GetAllowedBranchIds());
                if (parcelIds.Any())
                {
                    
                    model =  _invoiceService.GetCommercialInvoiceViewModels(parcelIds,GetAllowedBranchIds());
                }
                
            }
            else
            {
                parcelIds = await _searchService.GetParcelIdsFromAdvancedSearch(searchQuery,GetAllowedBranchIds());
                if (parcelIds.Any())
                {
                    model =  _invoiceService.GetCommercialInvoiceViewModels(parcelIds,GetAllowedBranchIds());
                }
            }
            

            _logger.LogInformation($"View All Invoices complete {GetUserName()}");
            if (model != null)
                return PartialView("/Views/Invoice/DefaultScreen.cshtml", (await model).ToImmutableList());
            return PartialView("/Views/Invoice/DefaultScreen.cshtml", new List<CommercialInvoiceViewModel>());
        }

        //[ValidateAntiForgeryToken]
        [HttpPost]
        [Route("Invoice/ExportAll")]
        public async Task<FileResult> ExportAll(string data)
        {
            bool hasMultipleParcels;
            AdvancedQuery searchQuery = JsonConvert.DeserializeObject<AdvancedQuery>(data);
            IEnumerable<CommercialInvoiceViewModel> model = new List<CommercialInvoiceViewModel>();
            _logger.LogInformation($"Export All Invoices initiated {GetUserName()} - {JsonConvert.SerializeObject(searchQuery)}");
            if (searchQuery is null) return null;
            IEnumerable<int> parcelIds;
            if (!searchQuery.UseAdvancedSearch)
            {
                parcelIds = await _searchService.GetParcelIdsFromSimpleSearch(searchQuery,GetAllowedBranchIds());
                if (parcelIds.Any())
                {
                    model = await _invoiceService.GetCommercialInvoiceViewModels(parcelIds,GetAllowedBranchIds());
                }
                
            }
            else
            {
               
                parcelIds = await _searchService.GetParcelIdsFromAdvancedSearch(searchQuery,GetAllowedBranchIds());
                if (parcelIds.Any())
                {

                    model = await _invoiceService.GetCommercialInvoiceViewModels(parcelIds,GetAllowedBranchIds());
                }
            }
            _generatePdf.SetConvertOptions(new ConvertOptions
            {
                IsLowQuality = true,
                
            });


            _logger.LogInformation($"Export All Invoices complete {GetUserName()}");
            var pdf = await _generatePdf.GetByteArray("/Views/Invoice/Default.cshtml", model);
            return File(pdf, "application/pdf", GetFileName(model));
            //return await _generatePdf.GetPdf("/Views/Invoice/Default.cshtml",model);
        }

        private static string GetFileName(IEnumerable<CommercialInvoiceViewModel> model)
        {
            //var fileName = model.Count()>1 ? $"Invoice_{DateTime.UtcNow:yyyyMMddHHmm}.pdf" : $"Invoice_{model.FirstOrDefault().FinalMileReference}_{DateTime.UtcNow:yyyyMMddhhmm}.pdf";
            var fileName = $"Invoice_{model.FirstOrDefault().FinalMileReference}_{DateTime.UtcNow:yyyyMMddhhmm}.pdf";
            return fileName;
        }

    }
}