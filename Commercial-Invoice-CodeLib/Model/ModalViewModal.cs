﻿#region Copyright © 2021 Asendia
// All rights are reserved. Reproduction or transmission in whole or in part,
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
// 
// Filename: ModalViewModal.cs
// Date:     02/03/2021
// Author:   jamie.buchan
#endregion

namespace Commercial_Invoice_CodeLib.Model
{
    public struct ModalViewModal
    {
        public string Header { get; set; }
        public string Body { get; set; }
        public string Footer { get; set; }
    }
}