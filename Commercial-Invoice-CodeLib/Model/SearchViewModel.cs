﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Commercial_Invoice_CodeLib.Model
{
    public struct SearchViewModel
    {
        public int Total  { get; set; }//=> Rows.Count();

        public int TotalNotFiltered { get; set; }
        public IEnumerable<SearchResult> Rows { get; set; }
    }
}
