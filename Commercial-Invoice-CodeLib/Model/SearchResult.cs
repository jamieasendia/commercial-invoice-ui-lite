﻿#region Copyright © 2021 Asendia
// All rights are reserved. Reproduction or transmission in whole or in part,
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
// 
// Filename: SearchResult.cs
// Date:     24/02/2021
// Author:   jamie.buchan
#endregion

using System;

namespace Commercial_Invoice_CodeLib.Model
{
    public struct SearchResult
    {
        public int ParcelId { get; set; }
        public string RetailerParcelCode { get; set; }
        public string CarrierConsignmentCode { get; set; }
        public string RetailerOrderReference { get; set; }
        public string ReceiverName { get; set; }
        public string SenderName { get; set; }
        public string Country { get; set; }
        public string VehicleRegistrationNumber { get; set; }
        public string TrailerReference { get; set; }
        public string SealNumber { get; set; }
        public string ImoNumber { get; set; }
        public DateTime DepartureDate { get; set; }
        public string Mawb { get; set; }
    }
}