﻿using System.Security.AccessControl;

namespace Commercial_Invoice_CodeLib.Model
{
    public struct RetailerNameAndAddress
    {
        public string RetailerName { get; set; }
        public string RetailerAddressLine1 { get; set; }
        public string RetailerAddressLine2 { get; set; }
        public string RetailerCity { get; set; }
        public string RetailerState { get; set; }
        public string RetailerPostcode { get; set; }
        public string RetailerCountry { get; set; }
        public string HtmlString =>string.Join("<br/>",RetailerName,RetailerAddressLine1,RetailerAddressLine2,RetailerCity,RetailerState,RetailerPostcode,RetailerCountry);
        
    }


}