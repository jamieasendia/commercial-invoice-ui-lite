﻿using System;
using System.Collections.Generic;

namespace Commercial_Invoice_CodeLib.Model
{
    public struct CommercialInvoiceItem:IEqualityComparer<CommercialInvoiceItem>
    {
        public int ParcelItemId { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public string Composition { get; set; }
        public string Origin { get; set; }
        public string Currency { get; set; }
        public decimal Value { get; set; }
        public string EccnRef { get; set; }

      public bool Equals(CommercialInvoiceItem x, CommercialInvoiceItem y)
      {
          return x.ParcelItemId == y.ParcelItemId && x.Quantity == y.Quantity && x.Description == y.Description && x.Composition == y.Composition && x.Origin == y.Origin && x.Currency == y.Currency && x.Value == y.Value && x.EccnRef == y.EccnRef;
      }

      public int GetHashCode(CommercialInvoiceItem obj)
      {
          return HashCode.Combine(obj.ParcelItemId, obj.Quantity, obj.Description, obj.Composition, obj.Origin, obj.Currency, obj.Value, obj.EccnRef);
      }
    }
}