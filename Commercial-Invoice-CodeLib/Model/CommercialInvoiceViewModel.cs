﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace Commercial_Invoice_CodeLib.Model
{
    public record CommercialInvoiceViewModel:CommercialInvoice
    {
        public RetailerNameAndAddress ShipperNameAndAddress { get; set; }
        public ConsigneeNameAndAddress ConsigneeNameAndAddress { get; set; }
        public IImmutableList<CommercialInvoiceItem> CommercialInvoiceItems { get; set; }
    }
}