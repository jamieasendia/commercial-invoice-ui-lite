﻿#region Copyright © 2021 Asendia
// All rights are reserved. Reproduction or transmission in whole or in part,
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
// 
// Filename: LoginModel.cs
// Date:     18/03/2021
// Author:   jamie.buchan
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Commercial_Invoice_CodeLib.Model
{
    public class LoginModel
    {
            [Required]
            [Display(Name="Username")]
            public string Username { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name="Password")]
            public string Password { get; set; }

    }

    public class ForgotPasswordModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name="Email")]
        public string Email { get; set; } 
        
        [Required]
        [DataType(DataType.EmailAddress)]
        [Compare("Email", ErrorMessage = "The email addresses do not match.")]
        [Display(Name="Confirm Email")]
        public string EmailConfirmation { get; set; }


    }

}