﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using DapperQueryBuilder;

namespace Commercial_Invoice_CodeLib.Service
{
    public interface ILookupService
    {
        Task<IEnumerable<string>> GetParentRetailerName(string query);
        Task<IEnumerable<string>> GetRetailerName(string query);
        Task<IEnumerable<string>> GetAllRetailerNames();
        Task<IEnumerable<string>> GetAllParentRetailerNames();
        Task<IEnumerable<Country>> GetAllCountries();
        Task<IEnumerable<string>> GetBranchNames(IEnumerable<int> branchIds);
        Task<string> GetCarrierConsignmentCode(int parcelId);

    }
}