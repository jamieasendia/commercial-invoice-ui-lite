﻿#region Copyright © 2021 Asendia

// All rights are reserved. Reproduction or transmission in whole or in part,
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
// 
// Filename: InvoiceService.cs
// Date:     25/02/2021
// Author:   jamie.buchan

#endregion

using System.Collections.Generic;
using System.Threading.Tasks;
using Commercial_Invoice_CodeLib.Model;

namespace Commercial_Invoice_CodeLib.Service
{
    public interface IInvoiceService
    {
        Task<CommercialInvoiceViewModel> GetCommercialInvoice(int parcelId, IEnumerable<int> allowedBranches);
        //Task<IEnumerable<CommercialInvoiceItem>> GetCommercialInvoiceItems(int parcelId);
        Task<IEnumerable<CommercialInvoiceViewModel>> GetCommercialInvoices(IEnumerable<int> parcelIds, IEnumerable<int> allowedBranches);
        Task<string> GetCommercialInvoiceHeader(int parcelId, IEnumerable<int> allowedBranches);
        Task<IEnumerable<CommercialInvoiceViewModel>> GetCommercialInvoiceViewModels(IEnumerable<int> parcelIds, IEnumerable<int> allowedBranches);
        //Task<NameAndAddress> GetRetailerAddress(int retailerAddressId);
        //Task<NameAndAddress> GetConsigneeAddress(int consigneeId);
    }
}