﻿#region Copyright © 2021 Asendia
// All rights are reserved. Reproduction or transmission in whole or in part,
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
// 
// Filename: InvoiceService.cs
// Date:     25/02/2021
// Author:   jamie.buchan
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Commercial_Invoice_CodeLib.Configuration;
using Commercial_Invoice_CodeLib.Model;
using Dapper;
using DapperQueryBuilder;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
// ReSharper disable StringLiteralTypo

namespace Commercial_Invoice_CodeLib.Service
{
    public class InvoiceService : ServiceBase<InvoiceService>, IInvoiceService
    {
 

        public async Task<CommercialInvoiceViewModel> GetCommercialInvoiceOld(int parcelId, IEnumerable<int> allowedBranches)
        {
	        await using var connection = new SqlConnection(_connectionString);
	        await connection.OpenAsync();

	        var sql = connection.QueryBuilder($@"Select 
		p.ParcelID as ParcelId,
		c.ConsigneeID as ConsigneeId,
		c.RetailerAddressID as RetailerAddressId,
		p.FinalMileReturnsReference as FinalMileReference,
		getdate() as DateTime,
		p.OriginalRetailerBarcode AS HouseAwb,
		p.TotalItems as Pieces,
		isnull(p.Width,0) as Width,
		isnull(p.Length,0) as Length,
		isnull(p.Height,0) as Height,
		c.Weight as Weight,
		ct.CountryCode as Country,
        c.ParcelCode as ParcelCode
	FROM 
		IFOS.MawbInformationTB (NOLOCK) mb
		Join IFOS.ScannedManifestTB sm (NOLOCK) ON sm.MawbInformationID = mb.MawbInformationID
		JOIN Container cn (NOLOCK) ON cn.ContainerNo = sm.ItemScanned
		JOIN Parcel p (NOLOCK) ON p.ContainerID = cn.ContainerID
		JOIN Consignment c (NOLOCK) ON c.ConsignmentID = p.ConsignmentID
		JOIN Country ct (NOLOCK) ON ct.CountryID = c.CountryID
	WHERE 
	p.ParcelID ={parcelId};");
            var searchResult = await sql.QuerySingleOrDefaultAsync<CommercialInvoiceViewModel>();// as CommercialInvoiceViewModel;
	        if (searchResult.ConsigneeId>0)searchResult.ConsigneeNameAndAddress = await GetConsigneeAddress(searchResult.ConsigneeId);
			if (searchResult.RetailerAddressId>0) searchResult.ShipperNameAndAddress = await GetRetailerAddress(searchResult.RetailerAddressId);
		    if (searchResult.ParcelId>0) searchResult.CommercialInvoiceItems = (await GetCommercialInvoiceItems(parcelId)).ToImmutableList();
  
		    return searchResult;

        }

        public async Task<CommercialInvoiceViewModel> GetCommercialInvoice(int parcelId, IEnumerable<int> allowedBranches)
        {
            var result =  await GetCommercialInvoiceViewModels(new[] {parcelId},allowedBranches);
            return result.OrderByDescending(x => x.DateTime).SingleOrDefault();
        }

        public async Task<IEnumerable<CommercialInvoiceViewModel>> GetCommercialInvoices(IEnumerable<int> parcelIds, IEnumerable<int> allowedBranches)
        {
            var result = new List<CommercialInvoiceViewModel>();
            foreach (var parcelId in parcelIds)
            {
                result.Add(await GetCommercialInvoice(parcelId,allowedBranches));   
            }

            return result;
        }

        public async Task<string> GetCommercialInvoiceHeader(int parcelId, IEnumerable<int> allowedBranches)
        {
             throw new NotImplementedException();
        }

        private async Task<IEnumerable<CommercialInvoiceItem>> GetCommercialInvoiceItems(int parcelId)
        {
	        await using var connection = new SqlConnection(_connectionString);
	        await connection.OpenAsync();

	        var sql = connection.QueryBuilder($@"SELECT 
		Quantity AS Quantity,
		Description as Description,
		Composition as Composition,
		CountryOfOrigin AS Origin,
		c.CurrencyCode as Currency,
		pi.Value as Value,
		pi.ExportControlClassificationNo as EccnRef
		FROM dbo.ParcelItem pi with (nolock)
		INNER JOIN dbo.Currency c with (nolock) on pi.CurrencyId = c.CurrencyId
		WHERE pi.ParcelId = {parcelId};");
	        var searchResult = await sql.QueryAsync<CommercialInvoiceItem>();
	        return searchResult;
        }

  
        private async Task<RetailerNameAndAddress> GetRetailerAddress(int retailerAddressId)
        {
	        await using var connection = new SqlConnection(_connectionString);
	        await connection.OpenAsync();

	        var sql = connection.QueryBuilder($@"select r.RetailerName as Name,
	ra.Address1 as AddressLine1, 
	ra.Address2 as AddressLine2,
	ra.City as City,
	ra.Postcode as Postcode,
	ct.CountryCode Country
	from dbo.RetailerAddress ra with (nolock) 
	inner join dbo.Retailer r with (nolock) on ra.RetailerID = r.RetailerID
	inner join dbo.country ct with (nolock) on ra.CountryId = ct.CountryId
	Where ra.RetailerAddressId = {retailerAddressId};");
	        var searchResult = await sql.QuerySingleOrDefaultAsync<RetailerNameAndAddress>();

	        return searchResult;
        }

        public async Task<ConsigneeNameAndAddress> GetConsigneeAddress(int consigneeId)
        {
	        await using var connection = new SqlConnection(_connectionString);
	        await connection.OpenAsync();

	        var sql = connection.QueryBuilder($@"SELECT 
		ce.Name as Name,
		ce.Address1 as AddressLine1,
		ce.Address2 as AddressLine2,
		ce.City as City,
		ce.State as State,
		ce.Postcode as Postcode,
		ct.CountryCode AS Country
		FROM dbo.Consignee ce with (nolock)
		INNER JOIN dbo.Country ct with (nolock)
		ON ce.CountryID = ct.countryid
		WHERE ce.ConsigneeID = {consigneeId};");
	        var searchResult = await sql.QuerySingleOrDefaultAsync<ConsigneeNameAndAddress>();

	        return searchResult;
        }


        private static void AddBranchFilter(QueryBuilder sql, IEnumerable<int> allowedBranches)
        {
            sql.AppendLine($" AND sm.BranchId in {allowedBranches}");
        }

        public async Task<IEnumerable<CommercialInvoiceViewModel>> GetCommercialInvoiceViewModels(IEnumerable<int> parcelId, IEnumerable<int> allowedBranches)
        {
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();
            List<CommercialInvoiceViewModel> result = new List<CommercialInvoiceViewModel>();
            var sql = $@"Select distinct p.ParcelId,
		p.MetapackTrackingCode as FinalMileReference,
		sm.DateTimeRecordCreated as DateTime,
		c.OrderNumber AS HouseAwb,
		p.TotalItems as Pieces,
		isnull(p.Width,0) as Width,
		isnull(p.Length,0) as Length,
		isnull(p.Height,0) as Height,
		p.plWeight as Weight,
		ct.CountryCode as Country,
        p.OriginalRetailerBarcode as ParcelCode,

        pi.ParcelItemId as ParcelItemId,
		pi.Quantity AS Quantity,
		pi.Description as Description,
		pi.Composition as Composition,
		pi.CountryOfOrigin AS Origin,
		cur.CurrencyCode as Currency,
		pi.Value as Value,
		pi.ExportControlClassificationNo as EccnRef,


		r.RetailerName as RetailerName,
		ra.Address1 as RetailerAddressLine1, 
		ra.Address2 as RetailerAddressLine2,
		ra.City as RetailerCity,
        ra.State as RetailerState,
		ra.Postcode as RetailerPostcode,
		ctret.CountryCode RetailerCountry,

		ce.Name as ConsigneeName,
		ce.Address1 as ConsigneeAddressLine1,
		ce.Address2 as ConsigneeAddressLine2,
		ce.City as ConsigneeCity,
		ce.State as ConsigneeState,
		ce.Postcode as ConsigneePostcode,
		ctcon.CountryCode AS ConsigneeCountry
	FROM 
		IFOS.MawbInformationTB (NOLOCK) mb
		Join IFOS.ScannedManifestTB sm (NOLOCK) ON sm.MawbInformationID = mb.MawbInformationID
		JOIN Container cn (NOLOCK) ON cn.ContainerNo = sm.ItemScanned
		JOIN Parcel p (NOLOCK) ON p.ContainerID = cn.ContainerID
		JOIN Consignment c (NOLOCK) ON c.ConsignmentID = p.ConsignmentID
		JOIN Country ct (NOLOCK) ON ct.CountryID = c.CountryID

		INNER join dbo.ParcelItem pi with (nolock) on p.ParcelID = pi.ParcelID
		INNER JOIN dbo.Currency cur with (nolock) on pi.CurrencyId = cur.CurrencyId

		INNER JOIN dbo.RetailerAddress ra with (nolock) on c.RetailerAddressID = ra.RetailerAddressID
		inner join dbo.Retailer r with (nolock) on ra.RetailerID = r.RetailerID
		inner join dbo.country ctret with (nolock) on ra.CountryId = ctret.CountryId

		INNER JOIN dbo.Consignee ce with (nolock) on c.ConsigneeID = ce.ConsigneeID
		INNER JOIN dbo.Country ctcon with (nolock) ON ce.CountryID = ctcon.countryid

	WHERE 
	p.ParcelID in @parcelids
    AND sm.branchid in @branchids
;";



            while (parcelId.Any())
            {
                var batchIds = parcelId.Take(1000);
                parcelId = parcelId.Skip(1000).ToList();
                var searchResult = await connection.QueryAsync<CommercialInvoiceResult>(sql,
                    new
                    {
                        parcelIds = batchIds,
                        branchIds = allowedBranches
                    }); // as CommercialInvoiceViewModel;

                var group = searchResult.GroupBy(result => result.ParcelId);


                foreach (var parcel in group)
                {
                    var newModel = new CommercialInvoiceViewModel
                    {
                        ParcelId = parcel.Key,
                        Country = parcel.First().Country,
                        DateTime = parcel.First().DateTime,
                        FinalMileReference = parcel.First().FinalMileReference,
                        ConsigneeNameAndAddress = new ConsigneeNameAndAddress
                        {
                            ConsigneeAddressLine1 = parcel.First().ConsigneeAddressLine1,
                            ConsigneeAddressLine2 = parcel.First().ConsigneeAddressLine2,
                            ConsigneeCity = parcel.First().ConsigneeCity,
                            ConsigneeCountry = parcel.First().ConsigneeCountry,
                            ConsigneeName = parcel.First().ConsigneeName,
                            ConsigneePostcode = parcel.First().ConsigneePostcode,
                            ConsigneeState = parcel.First().ConsigneeState
                        },
                        ShipperNameAndAddress = new RetailerNameAndAddress
                        {
                            RetailerAddressLine1 = parcel.First().RetailerAddressLine1,
                            RetailerAddressLine2 = parcel.First().RetailerAddressLine2,
                            RetailerCity = parcel.First().RetailerCity,
                            RetailerCountry = parcel.First().RetailerCountry,
                            RetailerName = parcel.First().RetailerName,
                            RetailerPostcode = parcel.First().RetailerPostcode,
                            RetailerState = parcel.First().RetailerState
                        },
                        Height = parcel.First().Height,
                        Width = parcel.First().Width,
                        Length = parcel.First().Length,
                        Weight = parcel.First().Weight,
                        HouseAwb = parcel.First().HouseAwb,
                        ParcelCode = parcel.First().ParcelCode,//parcel.First().ParcelCode,
                        Pieces = parcel.First().Pieces,
                        Total = parcel.Sum(x=>x.Quantity * x.Value),
                        /* CommercialInvoiceItems = results.Select(x => new CommercialInvoiceItem
                         {
                             ParcelItemId = x.ParcelItemId,
                             Composition = x.Composition,
                             Currency = x.Currency,
                             Description = x.Description,
                             EccnRef = x.EccnRef,
                             Origin = x.Origin,
                             Quantity = x.Quantity,
                             Value = x.Value
                         }).Distinct()*/
                    };
                    var items = parcel.Select(x => new CommercialInvoiceItem
                    {
                        ParcelItemId = x.ParcelItemId,
                        Composition = x.Composition,
                        Currency = x.Currency,
                        Description = x.Description,
                        EccnRef = x.EccnRef,
                        Origin = x.Origin,
                        Quantity = x.Quantity,
                        Value = x.Value
                    });

                    var itemGroup = items.GroupBy(x=>x.ParcelItemId );
                    var itemList = new List<CommercialInvoiceItem>();
                    foreach (var item in itemGroup)
                    {
                       itemList.Add(

                            new CommercialInvoiceItem
                            {
                                Value = item.First().Value,
                                Composition = item.First().Composition,
                                Currency = item.First().Currency,
                                Description = item.First().Description,
                                EccnRef = item.First().EccnRef,
                                Origin = item.First().Origin,
                                Quantity = item.First().Quantity,
                                ParcelItemId = item.Key
                            }
                        );
                    }

                    newModel.CommercialInvoiceItems = itemList.ToImmutableList();
                    result.Add(newModel);
                    //result.Add(new CommercialInvoiceViewModel());
                }
            }



            /*            result.AddRange(group.Select(results =>
                            new CommercialInvoiceViewModel
                            {
                                ParcelId = results.Key,
                                Country = results.First().Country,
                                DateTime = results.First().DateTime,
                                FinalMileReference = results.First().FinalMileReference,
                                ConsigneeNameAndAddress = new ConsigneeNameAndAddress
                                {
                                    ConsigneeAddressLine1 = results.First().ConsigneeAddressLine1,
                                    ConsigneeAddressLine2 = results.First().ConsigneeAddressLine2,
                                    ConsigneeCity = results.First().ConsigneeCity,
                                    ConsigneeCountry = results.First().ConsigneeCountry,
                                    ConsigneeName = results.First().ConsigneeName,
                                    ConsigneePostcode = results.First().ConsigneePostcode,
                                    ConsigneeState = results.First().ConsigneeState
                                },
                                ShipperNameAndAddress = new RetailerNameAndAddress
                                {
                                    RetailerAddressLine1 = results.First().RetailerAddressLine1,
                                    RetailerAddressLine2 = results.First().RetailerAddressLine2,
                                    RetailerCity = results.First().RetailerCity,
                                    RetailerCountry = results.First().RetailerCountry,
                                    RetailerName = results.First().RetailerName,
                                    RetailerPostcode = results.First().RetailerPostcode,
                                    RetailerState = results.First().RetailerState
                                },
                                Height = results.First().Height,
                                Width = results.First().Width,
                                Length = results.First().Length,
                                Weight = results.First().Weight,
                                HouseAwb = results.First().HouseAwb,
                                ParcelCode = results.First().ParcelCode,
                                Pieces = results.First().Pieces,
                                Total = results.First().Value,
                                CommercialInvoiceItems = results.Select(x => new CommercialInvoiceItem
                                {
                                    ParcelItemId = x.ParcelItemId,
                                    Composition = x.Composition,
                                    Currency = x.Currency,
                                    Description = x.Description,
                                    EccnRef = x.EccnRef,
                                    Origin = x.Origin,
                                    Quantity = x.Quantity,
                                    Value = x.Value
                                }).Distinct()
        
                            }
                        ));
                    }*/


            return result;

        }

        public InvoiceService(IOptionsMonitor<ConnectionStringSettings> xConnectionSettings, IOptionsMonitor<AppSettings> xAppSettings, ILogger<InvoiceService> logger, IMemoryCache memoryCache) 
            : base(xConnectionSettings,xAppSettings,logger,memoryCache)
        {
        }
    }

	
}