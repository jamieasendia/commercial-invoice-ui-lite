﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using PuppeteerSharp;

namespace Commercial_Invoice_CodeLib.Service
{
    public interface IConversionService
    {
        Task<byte[]> Convert(string html);
    }

  /*  public class PdfConversionService : IConversionService
    {
        public async Task<byte[]> Convert(string html)
        {
            await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision);
            var browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true
                
            });
            using var page = await browser.NewPageAsync();
            
                await page.SetContentAsync(html);
                //var result = await page.GetContentAsync();
                return await page.PdfDataAsync();
            
        }
    }*/
}
