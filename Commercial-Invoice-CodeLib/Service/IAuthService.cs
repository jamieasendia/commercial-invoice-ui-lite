﻿using System.Threading.Tasks;
using AsendiaShared.DTO.Entities;

namespace Commercial_Invoice_CodeLib.Service
{
    public interface IAuthService
    {

        Task<User> GetUserByLogin(string username);
        Task<bool> TryResetPassword(string emailAddress);
    }
}