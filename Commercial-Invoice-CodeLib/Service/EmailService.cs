﻿using System.Threading.Tasks;
using Commercial_Invoice_CodeLib.Configuration;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;

namespace Commercial_Invoice_CodeLib.Service
{
    public class EmailService : ServiceBase<EmailService>, IEmailService
    {
        private readonly IOptionsMonitor<NotificationMetadata> _emailOptions;
        
        public EmailService(IOptionsMonitor<ConnectionStringSettings> xConnectionSettings, IOptionsMonitor<AppSettings> xAppSettings, ILogger<EmailService> logger, IMemoryCache memoryCache, IOptionsMonitor<NotificationMetadata> NoxNotificationsMetaData, IOptionsMonitor<NotificationMetadata> emailOptions)
            : base(xConnectionSettings,xAppSettings,logger, memoryCache )
        {
            _emailOptions = emailOptions;
        }

        public async Task Send(string from, string to, string subject, string message)
        {
            // create message
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(from));
            email.To.Add(MailboxAddress.Parse(to));
            email.Subject = subject;
            email.Body = new TextPart(TextFormat.Html) { Text = message };

            // send email
            using var smtp = new SmtpClient();
            await smtp.ConnectAsync(_emailOptions.CurrentValue.SmtpServer,_emailOptions.CurrentValue.Port, SecureSocketOptions.StartTls);
            await smtp.AuthenticateAsync(_emailOptions.CurrentValue.Username,_emailOptions.CurrentValue.Password);
            await smtp.SendAsync(email);
            await smtp.DisconnectAsync(true);
        }
    }
}