﻿namespace Commercial_Invoice_CodeLib.Configuration
{
    public record ConnectionStringSettings
    {
        public string wnConsignDB { get; set; }
    }
}
