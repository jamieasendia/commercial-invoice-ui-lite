﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Commercial_Invoice_CodeLib.Helpers
{
    public static class ExtensionMethods
    {
        public static List<string> Roles(this ClaimsIdentity identity)
        {
            return identity.Claims
                .Where(c => c.Type == ClaimTypes.Role)
                .Select(c => c.Value)
                .ToList();
        }

        public static IEnumerable<string> GetBranches(this IEnumerable<Claim> claims)
        {
            return claims
                    .Where(c => c.Type == ClaimTypes.Role)
                    .Select(c => c.Value)
                ; //.ToList();
        }

     
    }
}
